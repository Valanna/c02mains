#include <stdio.h>
int	ft_str_is_alpha(char *str);

int	main(void)
{
	char *str = "123";
	char *str2 = "";
	char *str3 = "Hell0";
	char *str4 = "9876s4321";

	printf("%s | %i\n", str, ft_str_is_alpha(str));
	printf("%s | %i\n", str2, ft_str_is_alpha(str2));
	printf("%s | %i\n", str3, ft_str_is_alpha(str3));
	printf("%s | %i\n", str4, ft_str_is_alpha(str4));
}