#include <stdio.h>
#include <string.h>
#include <stdlib.h>
char	*ft_strncpy(char *dest, char *src, unsigned int n);

int	main(void)
{
	char	*strd;
	char	*strs;
	char	*strd2;
	char	*strs2;
	int		n = 3;

	strs = "Bonjour";
	strd = malloc(strlen(strs) * sizeof(strs));
	strs2 = strs;
	strd2 = strd;
	strncpy(strd, strs, n);
	printf("real: %s\n", strd);
	ft_strncpy(strd2, strs2, n);
	printf("ft: %s\n", strd2);
}